<?php
Class ScoreCounter {
    
    function showScore($aScore, $bScore) {
        $word_array = array(
            0 => 'Love',
            1 => '15',
            2 => '30',
            3 => '40',
            4 => 'Win'
        );

        if($this->isDueceMode($aScore, $bScore)) {
            if($aScore == $bScore) {
                return 'Duece:Duece';
            }
            elseif($aScore > $bScore) {
                return ($this->checkWin($aScore, $bScore) ? 'Win:Lose' : 'Adv.:-');
            }
            else {
                return ($this->checkWin($bScore, $aScore) ? 'Lose:Win' : '-:Adv.');
            }
        }
        else {
            if($aScore > $bScore) {
                if($this->checkWin($aScore, $bScore)) {
                    return 'Win:Lose';
                }
            }
            else {
                if($this->checkWin($bScore, $aScore)) {
                    return 'Lose:Win';
                }
            }
            return $word_array[$aScore] . ':' . $word_array[$bScore];
        }
        
            // if (adv) ? 'Adv.:-' :' -:Adv.';
            // if (winloase) ? 
    }

    function isDueceMode($aScore, $bScore) {
        return $aScore >= 3 && $bScore >= 3;
    }

    function checkWin($player1, $player2) {
        return $player1 - 2 == $player2;
    }

    // function checkWinner($aScore, $bScore) {
    //     $result = $aScore - $bScore;
    //     return ($result > 2 ? '');
    // }
}
?>