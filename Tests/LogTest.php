<?php
require_once(dirname(__FILE__) . '/../Log.php');
require_once(dirname(__FILE__) . '/../vendor/autoload.php');

class LogTest extends PHPUnit_Extensions_Database_TestCase {
    protected $pdo;

    public function getConnection() {
        try {
          $this->pdo = new PDO('sqlite:memory');
          $this->createTable($this->pdo);
          $this->truncateAutoIncrement($this->pdo);
          return $this->createDefaultDBConnection($this->pdo, ':memory:');
        }catch(PDOException $e) {
        }
      }

    protected function getDataset(){
        $primary = new PHPUnit_Extensions_Database_DataSet_YamlDataSet(dirname(__FILE__) .'/log.yaml');
        return $primary;
    }

    public function createTable(PDO $pdo) {
        $query = "
          CREATE TABLE access_log (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            ip varchar(15) NOT NULL,
            access_time datetime,
            response_time decimal(7,4),
            service_url varchar(50) NOT NULL DEFAULT 0
          );
        ";
        $pdo->query($query);
      }

      public function truncateAutoIncrement(PDO $pdo) {
        $query = "delete from sqlite_sequence where name='access_log';";
        $pdo->query($query);
      }

      public function testCountRow(){
        $log = new Log($this->pdo);
        $result = $log->countRow();
        $this->assertEquals(2, $result);
      }

      public function testInsert(){
        $log = new Log($this->pdo);
        $actualId = $log->createRow(['ip'=>"192.168.0.1", "access_time"=>"2014-03-12 12:14:20", "response_time"=>00.0020, "service_url"=>"/api/v3/captcha"]);
        $this->assertEquals('3', $actualId);
    }

}

?>