<?php
require_once(dirname(__FILE__) . '/../Random.php');

class TestRandom extends PHPUnit_Framework_TestCase {

    // function testNext() {
    //     $random = new Random();
    //     $this->assertEquals('2', $random->next());
    // }

    function testStubRandom() {
        $stub = $this->getMock('Random');

        $stub->expects($this->any())->method('next')->will($this->returnValue(2));
        $this->assertEquals(2, $stub->next(1, 9));
    }
}