<?php
require_once(dirname(__FILE__) . '/../ScoreCounter.php');

class ScoreCounterTest extends PHPUnit_Framework_TestCase {
    function testScoreCounter() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('Love:Love', $scoreCounter->showScore(0, 0));
    }

    function testShow15Love() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('15:Love', $scoreCounter->showScore(1, 0));
    }

    function testShow1515() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('15:15', $scoreCounter->showScore(1, 1));
    }

    function testDuece() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('Duece:Duece', $scoreCounter->showScore(3, 3));
    }

    function testDuece44() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('Duece:Duece', $scoreCounter->showScore(4, 4));
    }

    function testDuece55() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('Duece:Duece', $scoreCounter->showScore(5, 5));
    }

    function testScoreAfterDuece() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('Adv.:-', $scoreCounter->showScore(4, 3));
    }

    function testScoreAfterDuece45() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('-:Adv.', $scoreCounter->showScore(4, 5));
    }

    function testScoreAfterDuece56() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('-:Adv.', $scoreCounter->showScore(5, 6));
    }

    function testScoreWinAfterDueceAWin() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('Win:Lose', $scoreCounter->showScore(7, 5));
    }

    function testScoreWinAfterDueceBWin() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('Lose:Win', $scoreCounter->showScore(5, 7));
    }

    function testScoreWinAWin() {
        $scoreCounter = new ScoreCounter();
        $this->assertEquals('Win:Lose', $scoreCounter->showScore(4, 2));
    }
}

?>